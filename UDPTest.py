#! /usr/bin/python

import sys
import socket

if __name__ == '__main__':
    # Datagram (udp) socket
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        print 'Socket created'
    except socket.error, msg:
        print 'Failed to create socket. Error Code : ' + str(msg[0]) + ' Message ' + msg[1]
        sys.exit()
    HOST = '127.255.255.2'
    PORT = 9097

    # Bind socket to local host and port
    try:
        s.bind((HOST, PORT))
    except socket.error , msg:
        print 'Bind failed. Error Code : ' + str(msg[0]) + ' Message ' + msg[1]
        sys.exit()
    print 'Socket bind complete'

    while True:
        data, addr = s.recvfrom(1024)
        if not data:
            break
        print data
        print addr

    s.close()
